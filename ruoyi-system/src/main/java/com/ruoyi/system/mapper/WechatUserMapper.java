package com.ruoyi.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.domain.entity.WechatUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 15373
 * @description 针对表【wechat_user】的数据库操作Mapper
 * @createDate 2022-09-07 19:48:44
 * @Entity web.domain.WechatUser
 */

@Mapper
public interface WechatUserMapper extends BaseMapper<WechatUser> {

}
