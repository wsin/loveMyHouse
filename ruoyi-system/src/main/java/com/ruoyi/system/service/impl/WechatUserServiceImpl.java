package com.ruoyi.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.WechatUser;
import com.ruoyi.common.utils.AssertUtil;
import com.ruoyi.system.mapper.WechatUserMapper;
import com.ruoyi.system.service.WechatUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 15373
 * @description 针对表【wechat_user】的数据库操作Service实现
 * @createDate 2022-09-07 19:48:44
 */
@Service
public class WechatUserServiceImpl extends ServiceImpl<WechatUserMapper, WechatUser> implements WechatUserService {
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean submit(WechatUser wechatUser) {
        AssertUtil.isTrue(wechatUser != null, "微信用户不能为空");
        AssertUtil.isTrue(wechatUser.getOpenId() != null, "微信用户openId不能为空");
        AssertUtil.isTrue(StrUtil.isBlankIfStr(wechatUser.getId()), wechatUser, this::save);
        AssertUtil.isTrue(!StrUtil.isBlankIfStr(wechatUser.getId()), wechatUser, this::updateById);
        return true;
    }

    @Override
    public WechatUser getByOpenId(String openId) {
        return this.getOne(Wrappers.<WechatUser>lambdaQuery().eq(WechatUser::getOpenId, openId));
    }
}
