package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.domain.entity.WechatUser;

/**
 * @author 15373
 * @description 针对表【wechat_user】的数据库操作Service
 * @createDate 2022-09-07 19:48:44
 */
public interface WechatUserService extends IService<WechatUser> {

    boolean submit(WechatUser wechatUser);

    WechatUser getByOpenId(String openId);
}
