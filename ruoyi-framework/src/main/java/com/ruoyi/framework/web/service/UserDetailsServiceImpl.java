package com.ruoyi.framework.web.service;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.WechatUser;
import com.ruoyi.common.core.domain.entity.WxUser;
import com.ruoyi.system.service.WechatUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.UserStatus;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户验证处理
 *
 * @author ruoyi
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private static final String WX_USER = "wxUser:";

    public static final String WX_CHILD_USER = "wxChildUser:";
    @Autowired
    private ISysUserService userService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private SysPasswordService passwordService;
    @Autowired
    private WechatUserService wechatUserService;

    @Autowired
    private SysPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.contains(WX_USER)) {
            String openId = username.split(":")[1];
            WechatUser wechatUser = wechatUserService.getByOpenId(openId);
            if (StringUtils.isNull(wechatUser)) {
                log.info("登录用户：{} 不存在.", username);
                throw new ServiceException("登录用户：" + username + " 不存在");
            }
            WxUser wxUser = new WxUser();
            BeanUtils.copyProperties(wechatUser, wxUser);
            return createLoginUser(wxUser);
        } else if (username.contains(WX_CHILD_USER)) {
            String id = username.split(":")[1];
            return getUserDetails(username, id);
        } else {
            SysUser user = userService.selectUserByUserName(username);

            if (StringUtils.isNull(user)) {
                log.info("登录用户：{} 不存在.", username);
                throw new ServiceException("登录用户：" + username + " 不存在");
            } else if (UserStatus.DELETED.getCode().equals(user.getDelFlag())) {
                log.info("登录用户：{} 已被删除.", username);
                throw new ServiceException("对不起，您的账号：" + username + " 已被删除");
            } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
                log.info("登录用户：{} 已被停用.", username);
                throw new ServiceException("对不起，您的账号：" + username + " 已停用");
            }
            return createLoginUser(user);
        }
    }

    private UserDetails getUserDetails(String username, String id) {
        WechatUser wechatUser = wechatUserService.getById(id);
        if (StringUtils.isNull(wechatUser)) {
            log.info("登录用户：{} 不存在.", username);
            throw new ServiceException("登录用户：" + username + " 不存在");
        }
        WxUser wxUser = new WxUser();
        BeanUtils.copyProperties(wechatUser, wxUser);
        return createLoginUser(wxUser);
    }

    public UserDetails createLoginUser(SysUser user) {
        return new LoginUser(user.getUserId(), user.getDeptId(), user, permissionService.getMenuPermission(user));
    }

    public UserDetails createLoginUser(WxUser user) {
        user.setSessionKey(bCryptPasswordEncoder.encode("123456"));
        Set<String> permissions = new HashSet<>(16);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_WxUser"));
        return new LoginUser(user, permissions, authorities);
    }
}
