package com.ruoyi.web.mapper;

import com.ruoyi.web.domain.HouseUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 工会用户Mapper接口
 * 
 * @author ruoyi
 * @date 2022-09-09
 */
@Mapper
public interface HouseUserMapper 
{
    /**
     * 查询工会用户
     * 
     * @param id 工会用户主键
     * @return 工会用户
     */
    public HouseUser selectHouseUserById(Long id);

    /**
     * 查询工会用户列表
     * 
     * @param houseUser 工会用户
     * @return 工会用户集合
     */
    public List<HouseUser> selectHouseUserList(HouseUser houseUser);

    /**
     * 新增工会用户
     * 
     * @param houseUser 工会用户
     * @return 结果
     */
    public int insertHouseUser(HouseUser houseUser);

    /**
     * 修改工会用户
     * 
     * @param houseUser 工会用户
     * @return 结果
     */
    public int updateHouseUser(HouseUser houseUser);

    /**
     * 删除工会用户
     * 
     * @param id 工会用户主键
     * @return 结果
     */
    public int deleteHouseUserById(Long id);

    /**
     * 批量删除工会用户
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHouseUserByIds(Long[] ids);
}
