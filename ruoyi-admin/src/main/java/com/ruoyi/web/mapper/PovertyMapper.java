package com.ruoyi.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.domain.Poverty;

import java.util.List;

/**
 * 贫困人员信息表Mapper接口
 *
 * @author zhilin
 * @date 2022-09-07
 */
public interface PovertyMapper extends BaseMapper<Poverty> {
    /**
     * 查询贫困人员信息表
     *
     * @param id 贫困人员信息表主键
     * @return 贫困人员信息表
     */
    public Poverty selectPovertyById(Long id);

    /**
     * 查询贫困人员信息表列表
     *
     * @param poverty 贫困人员信息表
     * @return 贫困人员信息表集合
     */
    public List<Poverty> selectPovertyList(Poverty poverty);

    /**
     * 新增贫困人员信息表
     *
     * @param poverty 贫困人员信息表
     * @return 结果
     */
    public int insertPoverty(Poverty poverty);

    /**
     * 修改贫困人员信息表
     *
     * @param poverty 贫困人员信息表
     * @return 结果
     */
    public int updatePoverty(Poverty poverty);

    /**
     * 删除贫困人员信息表
     *
     * @param id 贫困人员信息表主键
     * @return 结果
     */
    public int deletePovertyById(Long id);

    /**
     * 批量删除贫困人员信息表
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePovertyByIds(Long[] ids);
}
