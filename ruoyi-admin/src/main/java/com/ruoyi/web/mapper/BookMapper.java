package com.ruoyi.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.domain.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 书本基本信息模块Mapper接口
 *
 * @author ruoyi
 * @date 2022-09-07
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {
    /**
     * 查询书本基本信息模块
     *
     * @param id 书本基本信息模块主键
     * @return 书本基本信息模块
     */
    public Book selectBookById(Long id);

    /**
     * 查询书本基本信息模块列表
     *
     * @param book 书本基本信息模块
     * @return 书本基本信息模块集合
     */
    public List<Book> selectBookList(Book book);

    /**
     * 新增书本基本信息模块
     *
     * @param book 书本基本信息模块
     * @return 结果
     */
    public int insertBook(Book book);

    /**
     * 修改书本基本信息模块
     *
     * @param book 书本基本信息模块
     * @return 结果
     */
    public int updateBook(Book book);

    /**
     * 删除书本基本信息模块
     *
     * @param id 书本基本信息模块主键
     * @return 结果
     */
    public int deleteBookById(Long id);

    /**
     * 批量删除书本基本信息模块
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookByIds(Long[] ids);

    Integer start(@Param("id") Integer id);

    Integer unStart(@Param("id") Integer id);
}
