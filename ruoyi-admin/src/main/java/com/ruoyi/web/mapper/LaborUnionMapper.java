package com.ruoyi.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.domain.LaborUnion;

import java.util.List;

/**
 * 工会信息表Mapper接口
 *
 * @author zhilin
 * @date 2022-09-07
 */
public interface LaborUnionMapper extends BaseMapper<LaborUnion> {
    /**
     * 查询工会信息表
     *
     * @param id 工会信息表主键
     * @return 工会信息表
     */
    public LaborUnion selectLaborUnionById(Long id);

    /**
     * 查询工会信息表列表
     *
     * @param laborUnion 工会信息表
     * @return 工会信息表集合
     */
    public List<LaborUnion> selectLaborUnionList(LaborUnion laborUnion);

    /**
     * 新增工会信息表
     *
     * @param laborUnion 工会信息表
     * @return 结果
     */
    public int insertLaborUnion(LaborUnion laborUnion);

    /**
     * 修改工会信息表
     *
     * @param laborUnion 工会信息表
     * @return 结果
     */
    public int updateLaborUnion(LaborUnion laborUnion);

    /**
     * 删除工会信息表
     *
     * @param id 工会信息表主键
     * @return 结果
     */
    public int deleteLaborUnionById(Long id);

    /**
     * 批量删除工会信息表
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLaborUnionByIds(Long[] ids);
}
