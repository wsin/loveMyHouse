package com.ruoyi.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.web.domain.Chapter;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 章节表Mapper接口
 *
 * @author ruoyi
 * @date 2022-09-07
 */
@Mapper
public interface ChapterMapper extends BaseMapper<Chapter> {
    /**
     * 查询章节表
     *
     * @param id 章节表主键
     * @return 章节表
     */
    public Chapter selectChapterById(Long id);

    /**
     * 查询章节表列表
     *
     * @param chapter 章节表
     * @return 章节表集合
     */
    public List<Chapter> selectChapterList(Chapter chapter);

    /**
     * 新增章节表
     *
     * @param chapter 章节表
     * @return 结果
     */
    public int insertChapter(Chapter chapter);

    /**
     * 修改章节表
     *
     * @param chapter 章节表
     * @return 结果
     */
    public int updateChapter(Chapter chapter);

    /**
     * 删除章节表
     *
     * @param id 章节表主键
     * @return 结果
     */
    public int deleteChapterById(Long id);

    /**
     * 批量删除章节表
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChapterByIds(Long[] ids);
}
