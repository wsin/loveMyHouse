package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.vo.BookVo;
import com.ruoyi.web.domain.Book;
import com.ruoyi.web.domain.Chapter;
import com.ruoyi.web.mapper.BookMapper;
import com.ruoyi.web.mapper.ChapterMapper;
import com.ruoyi.web.service.IBookService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 书本基本信息模块Service业务层处理
 *
 * @author ruoyi
 * @date 2022-09-07
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {

    @Autowired
    private ChapterMapper chapterMapper;

    /**
     * 查询书本基本信息模块
     *
     * @param id 书本基本信息模块主键
     * @return 书本基本信息模块
     */
    @Override
    public BookVo selectBookById(Long id) {
        BookVo bookVo = new BookVo();
        List<Chapter> chapters = chapterMapper.selectList(Wrappers.<Chapter>lambdaQuery().select(Chapter::getId,Chapter::getBid,Chapter::getTitle).eq(Chapter::getBid, id));
        bookVo.setChapterList(chapters);
        Book book = this.baseMapper.selectBookById(id);
        BeanUtils.copyProperties(book, bookVo);
        return bookVo;
    }

    /**
     * 查询书本基本信息模块列表
     *
     * @param book 书本基本信息模块
     * @return 书本基本信息模块
     */
    @Override
    public List<Book> selectBookList(Book book) {
        return this.baseMapper.selectBookList(book);
    }

    /**
     * 新增书本基本信息模块
     *
     * @param book 书本基本信息模块
     * @return 结果
     */
    @Override
    public int insertBook(Book book) {
        book.setCreateTime(DateUtils.getNowDate());
        return this.baseMapper.insertBook(book);
    }

    /**
     * 修改书本基本信息模块
     *
     * @param book 书本基本信息模块
     * @return 结果
     */
    @Override
    public int updateBook(Book book) {
        book.setUpdateTime(DateUtils.getNowDate());
        return this.baseMapper.updateBook(book);
    }

    /**
     * 批量删除书本基本信息模块
     *
     * @param ids 需要删除的书本基本信息模块主键
     * @return 结果
     */
    @Override
    public int deleteBookByIds(Long[] ids) {
        return this.baseMapper.deleteBookByIds(ids);
    }

    /**
     * 删除书本基本信息模块信息
     *
     * @param id 书本基本信息模块主键
     * @return 结果
     */
    @Override
    public int deleteBookById(Long id) {
        return this.baseMapper.deleteBookById(id);
    }
}
