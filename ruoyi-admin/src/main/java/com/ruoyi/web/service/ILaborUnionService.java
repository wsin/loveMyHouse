package com.ruoyi.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.web.domain.LaborUnion;

import java.util.List;

/**
 * 工会信息表Service接口
 *
 * @author zhilin
 * @date 2022-09-07
 */
public interface ILaborUnionService extends IService<LaborUnion> {
    /**
     * 查询工会信息表
     *
     * @param id 工会信息表主键
     * @return 工会信息表
     */
    public LaborUnion selectLaborUnionById(Long id);

    /**
     * 查询工会信息表列表
     *
     * @param laborUnion 工会信息表
     * @return 工会信息表集合
     */
    public List<LaborUnion> selectLaborUnionList(LaborUnion laborUnion);

    /**
     * 新增工会信息表
     *
     * @param laborUnion 工会信息表
     * @return 结果
     */
    public int insertLaborUnion(LaborUnion laborUnion);

    /**
     * 修改工会信息表
     *
     * @param laborUnion 工会信息表
     * @return 结果
     */
    public int updateLaborUnion(LaborUnion laborUnion);

    /**
     * 批量删除工会信息表
     *
     * @param ids 需要删除的工会信息表主键集合
     * @return 结果
     */
    public int deleteLaborUnionByIds(Long[] ids);

    /**
     * 删除工会信息表信息
     *
     * @param id 工会信息表主键
     * @return 结果
     */
    public int deleteLaborUnionById(Long id);
}
