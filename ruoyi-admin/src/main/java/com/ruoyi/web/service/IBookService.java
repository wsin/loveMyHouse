package com.ruoyi.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.vo.BookVo;
import com.ruoyi.web.domain.Book;

import java.util.List;

/**
 * 书本基本信息模块Service接口
 *
 * @author ruoyi
 * @date 2022-09-07
 */
public interface IBookService extends IService<Book> {
    /**
     * 查询书本基本信息模块
     *
     * @param id 书本基本信息模块主键
     * @return 书本基本信息模块
     */
    public BookVo selectBookById(Long id);

    /**
     * 查询书本基本信息模块列表
     *
     * @param book 书本基本信息模块
     * @return 书本基本信息模块集合
     */
    public List<Book> selectBookList(Book book);

    /**
     * 新增书本基本信息模块
     *
     * @param book 书本基本信息模块
     * @return 结果
     */
    public int insertBook(Book book);

    /**
     * 修改书本基本信息模块
     *
     * @param book 书本基本信息模块
     * @return 结果
     */
    public int updateBook(Book book);

    /**
     * 批量删除书本基本信息模块
     *
     * @param ids 需要删除的书本基本信息模块主键集合
     * @return 结果
     */
    public int deleteBookByIds(Long[] ids);

    /**
     * 删除书本基本信息模块信息
     *
     * @param id 书本基本信息模块主键
     * @return 结果
     */
    public int deleteBookById(Long id);
}
