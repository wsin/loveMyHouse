package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.web.domain.HouseUser;
import com.ruoyi.web.mapper.HouseUserMapper;
import com.ruoyi.web.service.IHouseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author: 尹靓宏
 * @date: 10:54 2022/9/9
 */
@Service
public class HouseUserServiceImpl implements IHouseUserService {


    @Autowired
    private HouseUserMapper houseUserMapper;

    /**
     * 查询工会用户
     *
     * @param id 工会用户主键
     * @return 工会用户
     */
    @Override
    public HouseUser selectHouseUserById(Long id) {
        return null;
    }

    /**
     * 查询工会用户列表
     *
     * @param houseUser 工会用户
     * @return 工会用户集合
     */
    @Override
    public List<HouseUser> selectHouseUserList(HouseUser houseUser) {
        return null;
    }

    /**
     * 新增工会用户
     *
     * @param houseUser 工会用户
     * @return 结果
     */
    @Override
    public int insertHouseUser(HouseUser houseUser) {
        return houseUserMapper.insertHouseUser(houseUser);
    }

    @Override
    public boolean saveBatch(Collection<HouseUser> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<HouseUser> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean updateBatchById(Collection<HouseUser> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdate(HouseUser entity) {
        return false;
    }

    @Override
    public HouseUser getOne(Wrapper<HouseUser> queryWrapper, boolean throwEx) {
        return null;
    }

    @Override
    public Map<String, Object> getMap(Wrapper<HouseUser> queryWrapper) {
        return null;
    }

    @Override
    public <V> V getObj(Wrapper<HouseUser> queryWrapper, Function<? super Object, V> mapper) {
        return null;
    }

    @Override
    public BaseMapper<HouseUser> getBaseMapper() {
        return null;
    }

    @Override
    public Class<HouseUser> getEntityClass() {
        return null;
    }
}
