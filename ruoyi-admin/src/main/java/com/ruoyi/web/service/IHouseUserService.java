package com.ruoyi.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.web.domain.HouseUser;

import java.util.List;

/**
 * 工会用户Service接口
 * 
 * @author ruoyi
 * @date 2022-09-09
 */
public interface IHouseUserService extends IService<HouseUser>
{
    /**
     * 查询工会用户
     * 
     * @param id 工会用户主键
     * @return 工会用户
     */
    public HouseUser selectHouseUserById(Long id);

    /**
     * 查询工会用户列表
     * 
     * @param houseUser 工会用户
     * @return 工会用户集合
     */
    public List<HouseUser> selectHouseUserList(HouseUser houseUser);

    /**
     * 新增工会用户
     * 
     * @param houseUser 工会用户
     * @return 结果
     */
    public int insertHouseUser(HouseUser houseUser);

}
