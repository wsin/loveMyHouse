package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.web.domain.LaborUnion;
import com.ruoyi.web.mapper.LaborUnionMapper;
import com.ruoyi.web.service.ILaborUnionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工会信息表Service业务层处理
 * 
 * @author zhilin
 * @date 2022-09-07
 */
@Service
public class LaborUnionServiceImpl extends ServiceImpl<LaborUnionMapper, LaborUnion> implements ILaborUnionService 
{

    /**
     * 查询工会信息表
     * 
     * @param id 工会信息表主键
     * @return 工会信息表
     */
    @Override
    public LaborUnion selectLaborUnionById(Long id)
    {
        return this.baseMapper.selectLaborUnionById(id);
    }

    /**
     * 查询工会信息表列表
     * 
     * @param laborUnion 工会信息表
     * @return 工会信息表
     */
    @Override
    public List<LaborUnion> selectLaborUnionList(LaborUnion laborUnion)
    {
        return this.baseMapper.selectLaborUnionList(laborUnion);
    }

    /**
     * 新增工会信息表
     * 
     * @param laborUnion 工会信息表
     * @return 结果
     */
    @Override
    public int insertLaborUnion(LaborUnion laborUnion)
    {
        return this.baseMapper.insertLaborUnion(laborUnion);
    }

    /**
     * 修改工会信息表
     * 
     * @param laborUnion 工会信息表
     * @return 结果
     */
    @Override
    public int updateLaborUnion(LaborUnion laborUnion)
    {
        return this.baseMapper.updateLaborUnion(laborUnion);
    }

    /**
     * 批量删除工会信息表
     * 
     * @param ids 需要删除的工会信息表主键
     * @return 结果
     */
    @Override
    public int deleteLaborUnionByIds(Long[] ids)
    {
        return this.baseMapper.deleteLaborUnionByIds(ids);
    }

    /**
     * 删除工会信息表信息
     * 
     * @param id 工会信息表主键
     * @return 结果
     */
    @Override
    public int deleteLaborUnionById(Long id)
    {
        return this.baseMapper.deleteLaborUnionById(id);
    }
}
