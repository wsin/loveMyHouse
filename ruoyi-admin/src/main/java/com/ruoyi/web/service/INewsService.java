package com.ruoyi.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.web.domain.News;

import java.util.List;

/**
 * 新闻表Service接口
 *
 * @author zhilin
 * @date 2022-09-07
 */
public interface INewsService extends IService<News> {
    /**
     * 查询新闻表
     *
     * @param nid 新闻表主键
     * @return 新闻表
     */
    public News selectNewsByNid(Long nid);

    /**
     * 查询新闻表列表
     *
     * @param news 新闻表
     * @return 新闻表集合
     */
    public List<News> selectNewsList(News news);

    /**
     * 新增新闻表
     *
     * @param news 新闻表
     * @return 结果
     */
    public int insertNews(News news);

    /**
     * 修改新闻表
     *
     * @param news 新闻表
     * @return 结果
     */
    public int updateNews(News news);

    /**
     * 批量删除新闻表
     *
     * @param nids 需要删除的新闻表主键集合
     * @return 结果
     */
    public int deleteNewsByNids(Long[] nids);

    /**
     * 删除新闻表信息
     *
     * @param nid 新闻表主键
     * @return 结果
     */
    public int deleteNewsByNid(Long nid);
}
