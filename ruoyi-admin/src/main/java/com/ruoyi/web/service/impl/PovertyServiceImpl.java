package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.web.domain.Poverty;
import com.ruoyi.web.mapper.PovertyMapper;
import com.ruoyi.web.service.IPovertyService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 贫困人员信息表Service业务层处理
 *
 * @author zhilin
 * @date 2022-09-07
 */
@Service
public class PovertyServiceImpl extends ServiceImpl<PovertyMapper, Poverty> implements IPovertyService {
    /**
     * 查询贫困人员信息表
     *
     * @param id 贫困人员信息表主键
     * @return 贫困人员信息表
     */
    @Override
    public Poverty selectPovertyById(Long id) {
        return this.baseMapper.selectPovertyById(id);
    }

    /**
     * 查询贫困人员信息表列表
     *
     * @param poverty 贫困人员信息表
     * @return 贫困人员信息表
     */
    @Override
    public List<Poverty> selectPovertyList(Poverty poverty) {
        return this.baseMapper.selectPovertyList(poverty);
    }

    /**
     * 新增贫困人员信息表
     *
     * @param poverty 贫困人员信息表
     * @return 结果
     */
    @Override
    public int insertPoverty(Poverty poverty) {
        return this.baseMapper.insertPoverty(poverty);
    }

    /**
     * 修改贫困人员信息表
     *
     * @param poverty 贫困人员信息表
     * @return 结果
     */
    @Override
    public int updatePoverty(Poverty poverty) {
        return this.baseMapper.updatePoverty(poverty);
    }

    /**
     * 批量删除贫困人员信息表
     *
     * @param ids 需要删除的贫困人员信息表主键
     * @return 结果
     */
    @Override
    public int deletePovertyByIds(Long[] ids) {
        return this.baseMapper.deletePovertyByIds(ids);
    }

    /**
     * 删除贫困人员信息表信息
     *
     * @param id 贫困人员信息表主键
     * @return 结果
     */
    @Override
    public int deletePovertyById(Long id) {
        return this.baseMapper.deletePovertyById(id);
    }
}
