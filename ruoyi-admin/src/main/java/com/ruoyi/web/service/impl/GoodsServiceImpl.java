package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.web.domain.Goods;
import com.ruoyi.web.mapper.GoodsMapper;
import com.ruoyi.web.service.IGoodsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 尹靓宏
* @description 针对表【goods】的数据库操作Service实现
* @createDate 2022-09-09 00:28:43
*/
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods>
implements IGoodsService {

    /**
     * 查询新闻表
     *
     * @param gid 新闻表主键
     * @return 新闻表
     */
    @Override
    public Goods selectGoodsByGid(Long gid) {
        return this.baseMapper.selectGoodsByGid(gid);
    }

    /**
     * 查询新闻表列表
     *
     * @param goods 新闻表
     * @return 新闻表
     */
    @Override
    public List<Goods> selectGoodsList(Goods goods) {
        return this.baseMapper.selectGoodsList(goods);
    }

}
