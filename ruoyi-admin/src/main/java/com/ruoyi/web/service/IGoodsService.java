package com.ruoyi.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.web.domain.Goods;

import java.util.List;

/**
* @author 尹靓宏
* @description 针对表【goods】的数据库操作Service
* @createDate 2022-09-09 00:28:43
*/
public interface IGoodsService extends IService<Goods> {

    /**
     * 查询新闻表
     *
     * @param gid 新闻表主键
     * @return 新闻表
     */
    public Goods selectGoodsByGid(Long gid);

    /**
     * 查询新闻表列表
     *
     * @param goods 新闻表
     * @return 新闻表集合
     */
    public List<Goods> selectGoodsList(Goods goods);


}
