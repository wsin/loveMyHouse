package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.web.domain.Chapter;
import com.ruoyi.web.mapper.ChapterMapper;
import com.ruoyi.web.service.IChapterService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 章节表Service业务层处理
 *
 * @author ruoyi
 * @date 2022-09-07
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements IChapterService {
    /**
     * 查询章节表
     *
     * @param id 章节表主键
     * @return 章节表
     */
    @Override
    public Chapter selectChapterById(Long id) {
        return this.baseMapper.selectChapterById(id);
    }

    /**
     * 查询章节表列表
     *
     * @param chapter 章节表
     * @return 章节表
     */
    @Override
    public List<Chapter> selectChapterList(Chapter chapter) {
        return this.baseMapper.selectChapterList(chapter);
    }

    /**
     * 新增章节表
     *
     * @param chapter 章节表
     * @return 结果
     */
    @Override
    public int insertChapter(Chapter chapter) {
        return this.baseMapper.insertChapter(chapter);
    }

    /**
     * 修改章节表
     *
     * @param chapter 章节表
     * @return 结果
     */
    @Override
    public int updateChapter(Chapter chapter) {
        return this.baseMapper.updateChapter(chapter);
    }

    /**
     * 批量删除章节表
     *
     * @param ids 需要删除的章节表主键
     * @return 结果
     */
    @Override
    public int deleteChapterByIds(Long[] ids) {
        return this.baseMapper.deleteChapterByIds(ids);
    }

    /**
     * 删除章节表信息
     *
     * @param id 章节表主键
     * @return 结果
     */
    @Override
    public int deleteChapterById(Long id) {
        return this.baseMapper.deleteChapterById(id);
    }
}
