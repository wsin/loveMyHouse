package com.ruoyi.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.web.domain.News;
import com.ruoyi.web.mapper.NewsMapper;
import com.ruoyi.web.service.INewsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 新闻表Service业务层处理
 *
 * @author zhilin
 * @date 2022-09-07
 */
@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, News> implements INewsService {
    /**
     * 查询新闻表
     *
     * @param nid 新闻表主键
     * @return 新闻表
     */
    @Override
    public News selectNewsByNid(Long nid) {
        return this.baseMapper.selectNewsByNid(nid);
    }

    /**
     * 查询新闻表列表
     *
     * @param news 新闻表
     * @return 新闻表
     */
    @Override
    public List<News> selectNewsList(News news) {
        return this.baseMapper.selectNewsList(news);
    }

    /**
     * 新增新闻表
     *
     * @param news 新闻表
     * @return 结果
     */
    @Override
    public int insertNews(News news) {
        return this.baseMapper.insertNews(news);
    }

    /**
     * 修改新闻表
     *
     * @param news 新闻表
     * @return 结果
     */
    @Override
    public int updateNews(News news) {
        return this.baseMapper.updateNews(news);
    }

    /**
     * 批量删除新闻表
     *
     * @param nids 需要删除的新闻表主键
     * @return 结果
     */
    @Override
    public int deleteNewsByNids(Long[] nids) {
        return this.baseMapper.deleteNewsByNids(nids);
    }

    /**
     * 删除新闻表信息
     *
     * @param nid 新闻表主键
     * @return 结果
     */
    @Override
    public int deleteNewsByNid(Long nid) {
        return this.baseMapper.deleteNewsByNid(nid);
    }
}
