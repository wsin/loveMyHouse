package com.ruoyi.web.controller.union;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.WechatUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.AssertUtil;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.system.service.WechatUserService;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author zhilin
 * @since 2022/9/7 18:07
 */
@RestController
@RequestMapping("/wechat")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginController {

    private final WxMaService wxMaService;
    private final RedisCache redisCache;

    private final WechatUserService wechatUserService;

    private final SysLoginService sysLoginService;
    private static final String WECHAT_LOGIN_USER_KEY = "wechat_login_user_key:";

    @GetMapping("/login")
    public AjaxResult login(String code) throws WxErrorException {
        AssertUtil.isTrue(!StringUtils.isBlank(code), "code不能为空");
        WxMaJscode2SessionResult result = wxMaService.getUserService().getSessionInfo(code);
        return AjaxResult.success(result);
    }

    @GetMapping("/info")
    public AjaxResult info(String sessionKey, String signature, String rawData, String encryptedData, String iv, String openId, String unionId) {
        AssertUtil.isTrue(!StringUtils.isBlank(sessionKey), "sessionKey 不能为空");
        AssertUtil.isTrue(!StringUtils.isBlank(signature), "signature 不能为空");
        AssertUtil.isTrue(!StringUtils.isBlank(rawData), "rawData 不能为空");
        AssertUtil.isTrue(!StringUtils.isBlank(encryptedData), "encryptedData 不能为空");
        AssertUtil.isTrue(!StringUtils.isBlank(iv), "iv 不能为空");
        AssertUtil.isTrue(!StringUtils.isBlank(openId), "openId 不能为空");
        // 用户信息校验
        AssertUtil.isTrue(wxMaService.getUserService().checkUserInfo(sessionKey, rawData, signature), "用户信息校验失败");
        // 解密用户信息
        WxMaUserInfo userInfo = wxMaService.getUserService().getUserInfo(sessionKey, encryptedData, iv);
        redisCache.setCacheObject(WECHAT_LOGIN_USER_KEY + openId, userInfo, 60, TimeUnit.SECONDS);
        userInfo.setUnionId(unionId);
        WechatUser wechatUser = wechatUserService.getOne(Wrappers.<WechatUser>lambdaQuery().eq(WechatUser::getOpenId, openId));
        AssertUtil.isTrue(wechatUser != null, wechatUser, user -> {
            user.setNickName(user.getNickName() == null ? userInfo.getNickName() : user.getNickName());
            user.setGender(user.getGender() == null ? Integer.parseInt(userInfo.getGender()) : user.getGender());
            user.setAvatarUrl(userInfo.getAvatarUrl());
            user.setOpenId(openId);
            user.setUnionId(userInfo.getUnionId());
            AssertUtil.isTrue(wechatUserService.submit(user), "更新用户信息失败");
        });
        AssertUtil.isTrue(wechatUser == null, wechatUser, user -> {
            WechatUser newWechatUser = new WechatUser();
            BeanUtils.copyProperties(userInfo, newWechatUser);
            newWechatUser.setAvatarUrl(userInfo.getAvatarUrl());
            newWechatUser.setNickName(userInfo.getNickName());
            newWechatUser.setGender(Integer.parseInt(userInfo.getGender()));
            newWechatUser.setOpenId(openId);
            AssertUtil.isTrue(wechatUserService.submit(newWechatUser), "添加用户信息失败");
        });
        Map<String, Object> map = new HashMap<>(16);
        String token = sysLoginService.wxLogin(openId, null);
        map.put("token", token);
        redisCache.deleteObject(WECHAT_LOGIN_USER_KEY + openId);
        return AjaxResult.success(map);
    }
}
