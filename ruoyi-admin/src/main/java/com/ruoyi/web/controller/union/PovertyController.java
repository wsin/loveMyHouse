package com.ruoyi.web.controller.union;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.Poverty;
import com.ruoyi.web.service.IPovertyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 贫困人员信息表Controller
 *
 * @author zhilin
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/system/poverty")
public class PovertyController extends BaseController {
    @Autowired
    private IPovertyService povertyService;

    /**
     * 查询贫困人员信息表列表
     */
//    @PreAuthorize("@ss.hasPermi('system:poverty:list')")
    @GetMapping("/list")
    public TableDataInfo list(Poverty poverty) {
        startPage();
        List<Poverty> list = povertyService.selectPovertyList(poverty);
        return getDataTable(list);
    }

    /**
     * 导出贫困人员信息表列表
     */
    @PreAuthorize("@ss.hasPermi('system:poverty:export')")
    @Log(title = "贫困人员信息表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Poverty poverty) {
        List<Poverty> list = povertyService.selectPovertyList(poverty);
        ExcelUtil<Poverty> util = new ExcelUtil<Poverty>(Poverty.class);
        util.exportExcel(response, list, "贫困人员信息表数据");
    }

    /**
     * 获取贫困人员信息表详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:poverty:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(povertyService.selectPovertyById(id));
    }

    /**
     * 新增贫困人员信息表
     */
    @PreAuthorize("@ss.hasPermi('system:poverty:add')")
    @Log(title = "贫困人员信息表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Poverty poverty) {
        return toAjax(povertyService.insertPoverty(poverty));
    }

    /**
     * 修改贫困人员信息表
     */
    @PreAuthorize("@ss.hasPermi('system:poverty:edit')")
    @Log(title = "贫困人员信息表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Poverty poverty) {
        return toAjax(povertyService.updatePoverty(poverty));
    }

    /**
     * 删除贫困人员信息表
     */
    @PreAuthorize("@ss.hasPermi('system:poverty:remove')")
    @Log(title = "贫困人员信息表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(povertyService.deletePovertyByIds(ids));
    }
}
