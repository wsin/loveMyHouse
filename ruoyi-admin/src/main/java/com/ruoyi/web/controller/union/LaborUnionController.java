package com.ruoyi.web.controller.union;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.domain.LaborUnion;
import com.ruoyi.web.service.ILaborUnionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工会信息表Controller
 *
 * @author zhilin
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/system/union")
public class LaborUnionController extends BaseController {
    @Autowired
    private ILaborUnionService laborUnionService;

    /**
     * 查询工会信息表列表
     */
   // @PreAuthorize("@ss.hasPermi('system:union:list')")
    @GetMapping("/list")
    public TableDataInfo list(LaborUnion laborUnion) {
        startPage();
        List<LaborUnion> list = laborUnionService.selectLaborUnionList(laborUnion);
        return getDataTable(list);
    }

    /**
     * 导出工会信息表列表
     */
    // @PreAuthorize("@ss.hasPermi('system:union:export')")
    @Log(title = "工会信息表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, LaborUnion laborUnion) {
        List<LaborUnion> list = laborUnionService.selectLaborUnionList(laborUnion);
        ExcelUtil<LaborUnion> util = new ExcelUtil<LaborUnion>(LaborUnion.class);
        util.exportExcel(response, list, "工会信息表数据");
    }

    /**
     * 获取工会信息表详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:union:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(laborUnionService.selectLaborUnionById(id));
    }

    /**
     * 新增工会信息表
     */
    // @PreAuthorize("@ss.hasPermi('system:union:add')")
    @Log(title = "工会信息表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LaborUnion laborUnion) {
        return toAjax(laborUnionService.insertLaborUnion(laborUnion));
    }

    /**
     * 修改工会信息表
     */
    // @PreAuthorize("@ss.hasPermi('system:union:edit')")
    @Log(title = "工会信息表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LaborUnion laborUnion) {
        return toAjax(laborUnionService.updateLaborUnion(laborUnion));
    }

    /**
     * 删除工会信息表
     */
    // @PreAuthorize("@ss.hasPermi('system:union:remove')")
    @Log(title = "工会信息表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(laborUnionService.deleteLaborUnionByIds(ids));
    }
}
