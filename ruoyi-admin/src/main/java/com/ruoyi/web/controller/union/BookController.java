package com.ruoyi.web.controller.union;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.domain.Book;
import com.ruoyi.web.mapper.BookMapper;
import com.ruoyi.web.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 书本基本信息模块Controller
 *
 * @author ruoyi
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/web/book")
public class  BookController extends BaseController {
    @Autowired
    private IBookService bookService;
    @Autowired
    private BookMapper bookMapper;

    /**
     * 查询书本基本信息模块列表
     */
//    @PreAuthorize("@ss.hasPermi('system:book:list')")
    @GetMapping("/list")
    public TableDataInfo list(Book book) {
        startPage();
        List<Book> list = bookService.selectBookList(book);
        return getDataTable(list);
    }

    /**
     * 根据书籍名称模糊查询
     */
    @GetMapping("/book_list_by_name")
    public AjaxResult listByName(String name){
        return AjaxResult.success(bookService.list(Wrappers.<Book>lambdaQuery().select(Book::getId,Book::getName,Book::getAuthor,Book::getCover,Book::getBrowse,Book::getStar,Book::getDescription, BaseEntity::getCreateTime,BaseEntity::getUpdateTime,Book::getIsDel).like(!ObjectUtils.isEmpty(name),Book::getName,name)));
    }

    /**
     * 点击增加浏览量
     */
    @PutMapping("/views/{id}")
    public AjaxResult addViews(@PathVariable Integer id){
        LambdaUpdateWrapper<Book> updateWrapper = new LambdaUpdateWrapper<>();
        Book book = bookService.getOne(Wrappers.<Book>lambdaQuery().select(Book::getId,Book::getName,Book::getAuthor,Book::getCover,Book::getBrowse,Book::getStar,Book::getDescription, BaseEntity::getCreateTime,BaseEntity::getUpdateTime,Book::getIsDel).eq(Book::getId,id));
        return AjaxResult.success(bookService.update(updateWrapper.set(Book::getBrowse,book.getBrowse()+1).eq(Book::getId,id)));
    }

    /**
     * 收藏
     */
    @PutMapping("/start/{id}")
    public AjaxResult start(@PathVariable Integer id){
        return AjaxResult.success(bookMapper.start(id));
    }

    /**
     * 取消收藏
     */
    @PutMapping("/un_start/{id}")
    public AjaxResult unStart(@PathVariable Integer id){
        return AjaxResult.success(bookMapper.unStart(id));
    }

    /**
     * 导出书本基本信息模块列表
     */
    @PreAuthorize("@ss.hasPermi('system:book:export')")
    @Log(title = "书本基本信息模块", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Book book) {
        List<Book> list = bookService.selectBookList(book);
        ExcelUtil<Book> util = new ExcelUtil<Book>(Book.class);
        util.exportExcel(response, list, "书本基本信息模块数据");
    }

    /**
     * 获取书本基本信息模块详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:book:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(bookService.selectBookById(id));
    }

    /**
     * 新增书本基本信息模块
     */
    @PreAuthorize("@ss.hasPermi('system:book:add')")
    @Log(title = "书本基本信息模块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Book book) {
        return toAjax(bookService.insertBook(book));
    }

    /**
     * 修改书本基本信息模块
     */
    @PreAuthorize("@ss.hasPermi('system:book:edit')")
    @Log(title = "书本基本信息模块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Book book) {
        return toAjax(bookService.updateBook(book));
    }

    /**
     * 删除书本基本信息模块
     */
    @PreAuthorize("@ss.hasPermi('system:book:remove')")
    @Log(title = "书本基本信息模块", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bookService.deleteBookByIds(ids));
    }
}
