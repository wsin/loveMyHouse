package com.ruoyi.web.controller.union;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.domain.Chapter;
import com.ruoyi.web.service.IChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 章节表Controller
 *
 * @author ruoyi
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/system/chapter")
public class ChapterController extends BaseController {
    @Autowired
    private IChapterService chapterService;

    /**
     * 查询章节表列表
     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:list')")
    @GetMapping("/list")
    public TableDataInfo list(Chapter chapter) {
        startPage();
        List<Chapter> list = chapterService.selectChapterList(chapter);
        return getDataTable(list);
    }

    /**
     * 导出章节表列表
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:export')")
    @Log(title = "章节表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Chapter chapter) {
        List<Chapter> list = chapterService.selectChapterList(chapter);
        ExcelUtil<Chapter> util = new ExcelUtil<Chapter>(Chapter.class);
        util.exportExcel(response, list, "章节表数据");
    }

    /**
     * 获取章节表详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chapterService.selectChapterById(id));
    }

    /**
     * 根据书籍id查询对应所有的章节数据
     */
    @GetMapping("/chapter_list")
    public AjaxResult chapterListByBid(Integer bId){
        return AjaxResult.success(chapterService.list(Wrappers.<Chapter>lambdaQuery().select(Chapter::getId,Chapter::getTitle).eq(Chapter::getBid,bId)));
    }

    /**
     * 新增章节表
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:add')")
    @Log(title = "章节表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Chapter chapter) {
        return toAjax(chapterService.insertChapter(chapter));
    }

    /**
     * 修改章节表
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:edit')")
    @Log(title = "章节表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Chapter chapter) {
        return toAjax(chapterService.updateChapter(chapter));
    }

    /**
     * 删除章节表
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:remove')")
    @Log(title = "章节表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chapterService.deleteChapterByIds(ids));
    }
}
