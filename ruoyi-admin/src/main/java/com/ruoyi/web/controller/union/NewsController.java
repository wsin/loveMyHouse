package com.ruoyi.web.controller.union;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.domain.News;
import com.ruoyi.web.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 新闻表Controller
 *
 * @author zhilin
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/system/news")
public class NewsController extends BaseController {
    @Autowired
    private INewsService newsService;


//   @PreAuthorize("@ss.hasPermi('system:news:list')")
//     @GetMapping("/list")
//     public TableDataInfo list(News news) {
//         startPage();
//         List<News> list = newsService.selectNewsList(news);
//         return getDataTable(list);
//     }

    /**
     * 查询新闻表列表
     */
    @GetMapping("/list")
    public AjaxResult list() {
        return AjaxResult.success(newsService.list(Wrappers.<News>lambdaQuery().select(News::getNid,News::getNtitle,News::getNimage,News::getNtime)));
    }

    /**
     * 导出新闻表列表
     */
    @PreAuthorize("@ss.hasPermi('system:news:export')")
    @Log(title = "新闻表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, News news) {
        List<News> list = newsService.selectNewsList(news);
        ExcelUtil<News> util = new ExcelUtil<News>(News.class);
        util.exportExcel(response, list, "新闻表数据");
    }

    /**
     * 获取新闻表详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:news:query')")
    @GetMapping(value = "/{nid}")
    public AjaxResult getInfo(@PathVariable("nid") Long nid) {
        return AjaxResult.success(newsService.selectNewsByNid(nid));
    }

    /**
     * 新增新闻表
     */
    @PreAuthorize("@ss.hasPermi('system:news:add')")
    @Log(title = "新闻表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody News news) {
        return toAjax(newsService.insertNews(news));
    }

    /**
     * 修改新闻表
     */
    @PreAuthorize("@ss.hasPermi('system:news:edit')")
    @Log(title = "新闻表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody News news) {
        return toAjax(newsService.updateNews(news));
    }

    /**
     * 删除新闻表
     */
    @PreAuthorize("@ss.hasPermi('system:news:remove')")
    @Log(title = "新闻表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{nids}")
    public AjaxResult remove(@PathVariable Long[] nids) {
        return toAjax(newsService.deleteNewsByNids(nids));
    }
}
