package com.ruoyi.web.controller.union;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.web.domain.HouseUser;
import com.ruoyi.web.service.IHouseUserService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 工会用户Controller
 *
 * @author ruoyi
 * @date 2022-09-09
 */
@RestController
@RequestMapping("/system/user")
public class HouseUserController
{
    @Autowired
    private IHouseUserService houseUserService;


    /**
     * 新增工会用户
     */
    @Log(title = "工会用户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody HouseUser houseUser)
    {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if (houseUserService.getOne(Wrappers.<HouseUser>lambdaQuery().eq(HouseUser::getVId, loginUser.getWxUser().getId())) == null){
            return AjaxResult.error("您已经入会了");
        }
        houseUser.setVId(loginUser.getWxUser().getId());
        return houseUserService.insertHouseUser(houseUser)>0 ? AjaxResult.success("注册成功" ) : AjaxResult.error("注册失败");
    }
}
