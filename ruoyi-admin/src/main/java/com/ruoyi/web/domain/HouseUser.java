package com.ruoyi.web.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 工会用户对象 house_user
 * 
 * @author ruoyi
 * @date 2022-09-09
 */
@Data
public class HouseUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 身份证 */
    @Excel(name = "身份证")
    private String card;

    /** 用户id */
    @Excel(name = "用户id")
    private String vId;


}
