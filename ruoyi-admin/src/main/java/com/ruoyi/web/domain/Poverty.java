package com.ruoyi.web.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 贫困人员信息表对象 poverty
 *
 * @author zhilin
 * @date 2022-09-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Poverty extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String title;

    /**
     * 图片
     */
    @Excel(name = "图片")
    private String img;

    /**
     * 内容
     */
    @Excel(name = "内容")
    private String content;
}
