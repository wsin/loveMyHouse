package com.ruoyi.web.domain;

import java.io.Serializable;

/**
 * @TableName goods
 */
public class Goods implements Serializable {
    /**
     *
     */
    private Integer gid;

    /**
     *
     */
    private String gname;

    /**
     *
     */
    private String gbody;

    /**
     *
     */
    private String img;

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public Integer getGid() {
        return gid;
    }

    /**
     *
     */
    public void setGid(Integer gid) {
        this.gid = gid;
    }

    /**
     *
     */
    public String getGname() {
        return gname;
    }

    /**
     *
     */
    public void setGname(String gname) {
        this.gname = gname;
    }

    /**
     *
     */
    public String getGbody() {
        return gbody;
    }

    /**
     *
     */
    public void setGbody(String gbody) {
        this.gbody = gbody;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Goods other = (Goods) that;
        return (this.getGid() == null ? other.getGid() == null : this.getGid().equals(other.getGid()))
                && (this.getGname() == null ? other.getGname() == null : this.getGname().equals(other.getGname()))
                && (this.getGbody() == null ? other.getGbody() == null : this.getGbody().equals(other.getGbody()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getGid() == null) ? 0 : getGid().hashCode());
        result = prime * result + ((getGname() == null) ? 0 : getGname().hashCode());
        result = prime * result + ((getGbody() == null) ? 0 : getGbody().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", gid=").append(gid);
        sb.append(", gname=").append(gname);
        sb.append(", img=").append(img);
        sb.append(", gbody=").append(gbody);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}