package com.ruoyi.common.core.domain.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName wechat_user
 */
@Data
public class WechatUser implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 微信小程序用户唯一标识
     */
    private String openId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 1男 0女
     */
    private Integer gender;

    /**
     * 头像地址
     */
    private String avatarUrl;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 个人简介
     */
    private String introduction;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String isDel;

    private static final long serialVersionUID = 1L;

    private String unionId;

}