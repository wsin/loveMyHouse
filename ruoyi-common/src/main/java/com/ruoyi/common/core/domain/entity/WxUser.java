package com.ruoyi.common.core.domain.entity;

import lombok.Data;

/**
 * 微信小程序用户
 *
 * @author zhilin
 */
@Data
public class WxUser {

    private String id;

    private String sessionKey;

    /**
     * 微信小程序用户唯一标识
     */
    private String openId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 1男 0女
     */
    private Integer gender;

    /**
     * 头像地址
     */
    private String avatarUrl;

    /**
     * 手机号
     */
    private String phoneNumber;
}
