package com.ruoyi.common.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 微信小程序配置
 *
 * @author LiYan
 */
@ConditionalOnClass(WxMaService.class)
@ConfigurationProperties(prefix = "wechat.miniapp")
@Component
@Data
public class WechatMaConfig {

    /**
     * 设置微信小程序的appId
     */
    private String appId;

    /**
     * 设置微信小程序的Secret
     */
    private String appSecret;

    @Bean
    @ConditionalOnMissingBean(WxMaService.class)
    public WxMaService wxMaService() {
        WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
        config.setAppid(appId);
        config.setSecret(appSecret);
        WxMaService service = new WxMaServiceImpl();
        service.setWxMaConfig(config);
        return service;

    }
}
