package com.ruoyi.common.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ruoyi.common.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Mybatis plus 基本字段自动填充
 *
 * @author LiYan
 */
@Component
@Slf4j
public class CustomerMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
        try {
            this.strictInsertFill(metaObject, "createBy", String.class, Optional.ofNullable(SecurityUtils.getLoginUser().getUsername()).orElse("admin"));
        } catch (Exception e) {
            this.strictInsertFill(metaObject, "createBy", String.class, "admin");
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
        try {
            this.strictUpdateFill(metaObject, "updateBy", String.class, Optional.ofNullable(SecurityUtils.getLoginUser().getUsername()).orElse("admin"));
        } catch (Exception e) {
            this.strictUpdateFill(metaObject, "updateBy", String.class, "admin");
        }
    }
}
