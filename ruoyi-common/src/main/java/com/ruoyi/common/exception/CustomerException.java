package com.ruoyi.common.exception;

/**
 * @author zhilin
 * @since 2022/9/7 18:17
 */
public class CustomerException extends RuntimeException {

    private static final long serialVersionUID = -5580158528451583051L;

    private final int code;
    private final String message;

    public CustomerException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
