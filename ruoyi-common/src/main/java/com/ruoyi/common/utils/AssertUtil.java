package com.ruoyi.common.utils;

import com.ruoyi.common.exception.CustomerException;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author zhilin
 */
public class AssertUtil {

    private AssertUtil() {
        
    }

    /**
     * 正确断言工具
     *
     * @param condition 条件表达式
     * @param message   错误时的消息
     */
    public static void isTrue(boolean condition, String message) {
        if (!condition) {
            // 抛出自定义异常
            throw new CustomerException(400, message);
        }
    }

    /**
     * 断言函数执行
     *
     * @param condition 条件表达式
     * @param parameter 参数
     * @param consumer  执行的函数
     * @param <T>       类型
     */
    public static <T> void isTrue(boolean condition, T parameter, Consumer<T> consumer) {
        if (condition) {
            consumer.accept(parameter);
        }
    }

    /**
     * 断言函数执行
     *
     * @param condition 条件表达式
     * @param parameter 参数
     * @param function  执行的函数
     * @param <T>       参数类型
     * @param <R>       返回值类型
     * @return <R>      返回值
     */
    public static <T, R> R isTrueCall(boolean condition, T parameter, Function<T, R> function, String message) {
        if (condition) {
            return function.apply(parameter);
        } else {
            throw new CustomerException(400, message);
        }
    }
}
